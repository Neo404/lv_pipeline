# Base image
FROM letsventure/build:latest


WORKDIR /usr/src/app

# Bundle app source
COPY . .

RUN npm install --build-from-source=bcrypt
RUN npm install --quite
RUN npm run build

EXPOSE 80

# start app
CMD [ "npm","start" ]