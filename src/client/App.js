import React from 'react';
import { hot } from 'react-hot-loader';
import './style.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {browserHistory, BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Navbar from '../client/components/NavBar'
import DashBoard from "./components/DashBoard";
import AifInvestors from "./components/AifInvestors";
import CaDocSend from "./components/CaDocSend";
import DocStatusOffline from "./components/DocStatusOffline";
import DocStatusOnline from "./components/DocStatusOnline";
import Finished from "./components/Finished";
import SignIn from './components/SignIn';
class App extends React.Component {
  render() {
    return (
        // later include in separte styling sheet
        <Router>
          <div className="app-background">
              <Navbar/>
              <Switch>
                  {/*<Route path='/dashboard' component = { DashBoard } />*/}
                  {/*<Route path='/aif_investors' component = { AifInvestors } />*/}
                  {/*<Route path='/ca_doc_send' component = { CaDocSend } />*/}
                  {/*<Route path='/doc_status_offline' component = { DocStatusOffline} />*/}
                  {/*<Route path='/doc_status_online' component = { DocStatusOnline } />*/}
                  {/*<Route path='/finished' component = { Finished } />*/}
                  <Route exact path='/' component={SignIn}/>
              </Switch>
          </div>
        </Router>
    );
  }
}

export default hot(module)(App);
